from email.policy import default
from django import template
from recipes.models import Recipe

register = template.Library()

def resize_to(ingredient, target):
    recipe = ingredient.recipe.servings
    amount = int(ingredient.amount)
    total = 0

    if recipe != None and target != None and int(target) >= 0:
        input_servings = int(target)
        total = (amount / recipe) * input_servings
        return total
    else:
        return ingredient.amount


register.filter(resize_to)
