from django.shortcuts import redirect
# from django.shortcuts import render
from django.views.generic.list import ListView
from django.views.generic.detail import DetailView
from django.views.generic.edit import CreateView, DeleteView, UpdateView
from django.urls import reverse_lazy
from django.contrib.auth.mixins import LoginRequiredMixin
from psycopg2 import IntegrityError
from recipes.forms import RatingForm
from recipes.models import USER_MODEL, Recipe, ShoppingItem, Ingredient
from django.views.decorators.http import require_http_methods



# def create_recipe(request):
#     if request.method == "POST" and RecipeForm:
#         form = RecipeForm(request.POST)
#         if form.is_valid():
#             recipe = form.save()
#             return redirect("recipe_detail", pk=recipe.pk)
#     elif RecipeForm:
#         form = RecipeForm()
#     else:
#         form = None
#     context = {
#         "form": form,
#     }
#     return render(request, "recipes/new.html", context)


# def change_recipe(request, pk):
#     if Recipe and RecipeForm:
#         instance = Recipe.objects.get(pk=pk)
#         if request.method == "POST":
#             form = RecipeForm(request.POST, instance=instance)
#             if form.is_valid():
#                 form.save()
#                 return redirect("recipe_detail", pk=pk)
#         else:
#             form = RecipeForm(instance=instance)
#     else:
#         form = None
#     context = {
#         "form": form,
#     }
#     return render(request, "recipes/edit.html", context)


# def show_recipe(request, pk):
#     context = {
#         "recipe": Recipe.objects.get(pk=pk) if Recipe else None,
#         "rating_form": RatingForm(),
#     }
#     return render(request, "recipes/detail.html", context)


def log_rating(request, recipe_id):
    if request.method == "POST":
        form = RatingForm(request.POST)
        if form.is_valid():
            rating = form.save(commit=False)
            try:
                rating.recipe = Recipe.objects.get(pk=recipe_id)
                rating.save()
            except Recipe.DoesNotExist:
                return redirect("recipes_list")
    return redirect("recipe_detail", pk=recipe_id)


class RecipeListView(ListView):
    model = Recipe
    # context_object_name = "recipe_list"
    template_name = "recipes/list.html"
    paginate_by = 3

    def get_queryset(self):
        query = self.request.GET.get("q")
        if not query:
            query = ""
        return Recipe.objects.filter(name__icontains=query)


class AuthorListView(ListView):
    model = USER_MODEL
    # context_object_name = "author_list"
    template_name = "recipes/users.html"
    paginate_by = 3

    #get queryset orderby function


class RecipeDetailView(DetailView):
    model = Recipe
    template_name = "recipes/detail.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["rating_form"] = RatingForm()

        serving = self.request.GET.get("servings")
        context["serving_size"] = serving

        food_items = []

        for item in self.request.user.shopping_items.all():
            food_items.append(item.food_item)
        context["food_in_shopping_list"] = food_items

        return context


class RecipeCreateView(LoginRequiredMixin, CreateView):
    model = Recipe
    template_name = "recipes/new.html"
    fields = ["name", "description", "servings", "image"]
    success_url = reverse_lazy("recipes_list")

    def form_valid(self, form):
        form.instance.author = self.request.user
        return super().form_valid(form)


class RecipeUpdateView(LoginRequiredMixin, UpdateView):
    model = Recipe
    template_name = "recipes/edit.html"
    fields = ["name", "description", "servings", "image"]
    success_url = reverse_lazy("recipes_list")

    def form_valid(self, form):
        form.instance.updated_by = self.request.user
        return super().form_valid(form)


class RecipeDeleteView(LoginRequiredMixin, DeleteView):
    model = Recipe
    template_name = "recipes/delete.html"
    success_url = reverse_lazy("recipes_list")


class ShoppingItemsListView(ListView):
    model = ShoppingItem
    template_name = "shopping_list/list.html"

    def get_queryset(self):
        return ShoppingItem.objects.filter(user=self.request.user)


@require_http_methods(["POST"])
def new_shopping_item(request):
    ingredient_id = request.POST.get("ingredient_id")
    print("id-----> " + ingredient_id)
    print(type(ingredient_id))
    ingredient = Ingredient.objects.get(id=ingredient_id)
    print("ingredient type-----> " + str(type(ingredient)))
    user = request.user
    print(request.user)
    try:
        ShoppingItem.objects.create(
            food_item=ingredient.food,
            user=user,
        )

    except IntegrityError:
        pass
    return redirect("recipe_detail", pk=ingredient.recipe.id)


def delete_shopping_items(request):
    ShoppingItem.objects.filter(user=request.user).delete()
    return redirect(
        "shopping_list"
    )

# # @require_http_methods(["POST"])
# def serving_form(request):
#     serving_size = request.POST.get()

# @register.filter(name='resize_to')
# def resize_to(self, num_servings):
#     servings_calculated = (Recipe.servings / Recipe.servings) * num_servings
#     return servings_calculated