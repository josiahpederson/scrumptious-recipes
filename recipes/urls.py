from django.urls import path

from recipes.views import (
    RecipeCreateView,
    RecipeDeleteView,
    RecipeUpdateView,
    log_rating,
    RecipeDetailView,
    RecipeListView,
    AuthorListView,
    ShoppingItemsListView,
    new_shopping_item,
    delete_shopping_items,
)

urlpatterns = [
    path("", RecipeListView.as_view(), name="recipes_list"),
    path("<int:pk>/", RecipeDetailView.as_view(), name="recipe_detail"),
    path("<int:pk>/delete/", RecipeDeleteView.as_view(), name="recipe_delete"),
    path("new/", RecipeCreateView.as_view(), name="recipe_new"),
    path("<int:pk>/edit/", RecipeUpdateView.as_view(), name="recipe_edit"),
    path("<int:recipe_id>/ratings/", log_rating, name="recipe_rating"),
    path("authors/", AuthorListView.as_view(), name="author_list"),
    path("shopping_items/new/", new_shopping_item, name="shopping_item_new"),
    path("shopping_items/", ShoppingItemsListView.as_view(), name="shopping_list"),
    path("shopping_items/delete/", delete_shopping_items, name="shopping_list_delete"),
]
